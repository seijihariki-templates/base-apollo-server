module.exports = {
  extends: ['airbnb-typescript/base'],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    "import/prefer-default-export": 0,
    "@typescript-eslint/no-unused-vars": 0,
    "class-methods-use-this": 0
  },
  parser: '@typescript-eslint/parser',
};
