# Base Apollo Server

A simple to use template for fast development of backend systems using the following main technologies:
 - Base Language : [Typescript](https://www.typescriptlang.org/) is used as the main language, with standard linter.
 - Query Language: [GraphQL](https://graphql.org/) is used as a query language, for structured API calls.
 - Database      : [MongoDB](https://www.mongodb.com/) is used as the main database.
 - Containers    : [Docker and Docker Compose](https://www.docker.com/) are used for deployment in production and development environments. The included docker-compose file is only suitable for development. It is recommended to use Kubernetes on actual production.


Some notable external libraries used for this template are:
 - [Type GraphQL](https://typegraphql.ml/), for annotation based GraphQL entities and authorization functionalities.
 - [DataLoader](https://github.com/graphql/dataloader/), for per-request caching for GraphQL.
 - [BCrypt.JS](https://github.com/dcodeIO/bcrypt.js), for user password hashing without dependencies.
 - [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken), for authentication token generation and validation.

## [Documentation](https://gitlab.com/seijihariki-templates/base-apollo-server/-/wikis/Documentation)

## What is included?

Below are some general topics and what is and what is not present in this template:

### Models

Models are extremely easy to create and change. Resolvers are also easily added. Models and resolvers are automatically imported, and watch is enabled in development. The _Indexable_ base type is great for creating new collections of models, and keeps things flexible for customization if needed. You will almost never have to deal with the MongoDB libraries directly. For more information refer to the wiki

### Users and Permissions

There are simple models and simple permissioning already built in with the template. Possible roles are hardcoded and users are as simple as they get, but they work. This system can be easily replaced with a custom and more complete one without problems. For more information refer to the wiki

### GraphQL Caching

Models based on the _Indexable_ base type are automatically cached by id per-request, avoiding multiple queries for the same object in a single request.

### GraphQL Subscriptions

Subscriptions are built-in and working out of the box, mainly due to the [Type GraphQL](https://typegraphql.ml/) framework. 

### What more?

Nothing, really. This is a template focused of simplicity and flexibility. Most of the features are either for optimization (Caching), or of generic necessity (Users and Permissions, Subscriptions). Users and Permissions were actually only added because the used framework (Type GraphQL) supported it. Also, adding more features would increase package size. For more plug-and-play functionality I would be willing to make separate modules.

## Credits

This project was initally based on [Liron Navon's](https://github.com/liron-navon) [tutorial on hashnode](https://hashnode.com/post/building-a-nodejs-api-with-typescript-and-graphql-cjrrojjx200uqrxs1ngtitx9p).

## License

[MIT](https://gitlab.com/seijihariki-templates/base-apollo-server/blob/master/LICENSE)
