import { logger } from '@/logging';
import { Db, MongoClient } from 'mongodb';
import { Category } from 'typescript-logging';

let db = null;
let client = null;

const logCategory = new Category('Database');

export const getDB = async (): Promise<{ client: MongoClient, db: Db }> => {
  if (db && client) {
    return { client, db };
  }

  const uri = process.env.MONGODB_URI || 'mongodb://db:27017/';
  const database = process.env.MONGODB_DATA || 'database';

  try {
    client = await MongoClient.connect(uri, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });

    db = client.db(database);

    logger.info(`Connected to '${database}' at '${uri}'`, logCategory);
  } catch (err) {
    logger.error(`Tried to connect using URI: '${uri}'`, err, logCategory);
  }

  return { client, db };
};
