import { Indexable } from '@/model/Indexable';
import dataloader from 'dataloader';
import { IncomingMessage, OutgoingMessage } from 'http';

export interface IRequestContext {
  // Loaders is required
  loaders: {
    [key: string]: dataloader<string, any extends Indexable<any> ? any : any>;
  };
  // Incoming HTTP Request
  req?: IncomingMessage;
  // Outgoing HTTP Request
  res?: OutgoingMessage;
  // User Auth token
  token?: string;
  // User ID
  user?: string;
  // User Roles
  roles?: string[];
  // User Permissions
  permissions?: string[];
  // Accept other context usages
  [key: string]: any;
}
