import {
  Arg, Authorized, Ctx, ID, Mutation, Query, Resolver,
} from 'type-graphql';

import { IRequestContext } from '@/middleware/context';

// Model
import { Greeting } from '@/model/greeting/Greeting.model';
import { GreetingCreateInput } from '@/model/greeting/GreetingCreateInput.model';
import { GreetingFilterInput } from '@/model/greeting/GreetingFilterInput.model';
import { GreetingUpdateInput } from '@/model/greeting/GreetingUpdateInput.model';
import { Role } from '@/model/user/User.model';

@Resolver()
export class GreetingsResolver {
  @Authorized(Role.READ)
  @Query((returns) => Greeting, {
    description: 'Retrieves greeting with given ID',
  })
  public greeting(
  @Ctx() ctx: IRequestContext,
    @Arg('id', (type) => ID) id: string,
  ) {
    return Greeting.load(ctx, id);
  }

  @Authorized(Role.READ)
  @Query((returns) => [Greeting], {
    description: 'Retrieves list of filtered greetings',
  })
  public async greetings(
  @Ctx() ctx: IRequestContext,
    @Arg('filter', { nullable: true }) filter?: GreetingFilterInput,
  ) {
    return Greeting.find(ctx, filter);
  }

  @Authorized(Role.WRITE)
  @Mutation((returns) => Greeting, {
    description: 'Creates a new greeting',
  })
  public createGreeting(
  @Ctx() ctx: IRequestContext,
    @Arg('data') data: GreetingCreateInput,
  ) {
    return Greeting.create(ctx, data);
  }

  @Authorized(Role.WRITE)
  @Mutation((returns) => Greeting, {
    description: 'Updates greeting with given ID',
  })
  public async updateGreeting(
  @Ctx() ctx: IRequestContext,
    @Arg('id', (type) => ID) id: string,
    @Arg('data') data: GreetingUpdateInput,
  ) {
    const obj: Greeting = await Greeting.load(ctx, id);

    obj.update(ctx, data);

    return obj;
  }
}
