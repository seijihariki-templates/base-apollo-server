import { IRequestContext } from '@/middleware/context';
import { ApolloError, ContextFunction } from 'apollo-server-core';
import cookie from 'cookie';
import { IncomingMessage, OutgoingMessage } from 'http';
import jwt from 'jsonwebtoken';
import jwksRsa from 'jwks-rsa';
import { ContextFactoryFunction } from './types.d';

export const JWKSContextFactory: ContextFactoryFunction<{ JWKS_SECRET_URI: string }> = (args) => {
  const secretFetch = jwksRsa({
    jwksUri: args.JWKS_SECRET_URI,
  });

  const context: ContextFunction<{
    req?: IncomingMessage, res?: OutgoingMessage,
  }, IRequestContext> = async ({ req, res }) => {
    let user: string;
    let roles: string[];
    let token;
    let permissions: string[];

    // WebSocket requests don't have the req parameter
    if (req) {
      const cookies = req.headers.cookie && cookie.parse(req.headers.cookie);

      // Authentication //
      // If authorization cookie found, verify and parse jwt
      // Handle cookie authentication
      if (cookies && cookies.authorization) {
        token = cookies.authorization;
      } else if (req.headers.authorization) {
        const [type, value] = req.headers.authorization.split(' ');

        if (type === 'Bearer') {
          token = value;
        }
      }

      if (token) {
        const decoded = jwt.decode(token, { complete: true }) as any;

        const key = await new Promise((resolve, reject) => {
          secretFetch.getSigningKey(decoded.header.kid, (err: Error, skey: jwksRsa.SigningKey) => {
            if (err) reject(err);

            resolve(skey);
          });
        });

        if (jwt.verify(token, (key as any).publicKey, { algorithms: ['RS256'] })) {
          user = decoded.payload.sub;
          roles = decoded.payload.roles;
          permissions = decoded.payload.permissions;
        } else {
          throw new ApolloError('Invalid JWT token', '400');
        }
      }
    }

    return {
      loaders: {},
      req,
      res,
      roles,
      permissions,
      session: null,
      token,
      user,
    };
  };
  return context;
};
