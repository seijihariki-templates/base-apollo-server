import { ContextFunction } from 'apollo-server-core';
import { IncomingMessage, OutgoingMessage } from 'http';
import { IRequestContext } from '@/middleware/context';

export type ContextFactoryFunction<T = { [key: string]: string }> = (args?: T) => ContextFunction<{
  req?: IncomingMessage, res?: OutgoingMessage,
}, IRequestContext>;
