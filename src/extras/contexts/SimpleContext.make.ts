import { IRequestContext } from '@/middleware/context';
import { ApolloError, ContextFunction } from 'apollo-server-core';
import cookie from 'cookie';
import { IncomingMessage, OutgoingMessage } from 'http';
import jwt from 'jsonwebtoken';
import { ContextFactoryFunction } from './types.d';

export const SimpleContextFactory: ContextFactoryFunction = () => {
  const context: ContextFunction<{
    req?: IncomingMessage, res?: OutgoingMessage,
  }, IRequestContext> = ({ req, res }) => {
    let user: string;
    let roles: [string];

    // WebSocket requests don't have the req parameter
    if (req) {
      const cookies = req.headers.cookie && cookie.parse(req.headers.cookie);

      // Authentication //
      // If authorization cookie found, verify and parse jwt
      {
        let token;
        // Handle cookie authentication
        if (cookies && cookies.authorization) {
          token = cookies.authorization;
        } else if (req.headers.authorization) {
          const [type, value] = req.headers.authorization.split(' ');

          if (type === 'Bearer') {
            token = value;
          }
        }

        if (token) {
          if (jwt.verify(token, process.env.JWT_SECRET || 'JWT_SECRET', { algorithms: ['HS256'] })) {
            const decoded = jwt.decode(token, { complete: true }) as any;

            user = decoded.payload.sub;
            roles = decoded.payload.roles;
          } else {
            throw new ApolloError('Invalid JWT token', '400');
          }
        }
      }
    }

    return {
      loaders: {},
      req,
      res,
      roles,
      session: null,
      user,
    };
  };

  return context;
};
