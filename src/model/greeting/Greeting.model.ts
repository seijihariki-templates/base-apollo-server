import { getDB } from '@/dataloader/db';
import { Field, ObjectType } from 'type-graphql';
import { Indexable } from '../Indexable';

@ObjectType({ description: 'Represents a greeting' })
export class Greeting extends Indexable<Greeting> {
  @Field({ description: 'Greeting human-readable ID' })
  public hrid: string;

  @Field({ description: 'Greeting template' })
  public template: string;

  @Field({ description: 'Greeting string representation' })
  public representation: string;

  // Required
  public getCollection() {
    return 'greetings';
  }
}

getDB().then(({ db }) => {
  const collection = db.collection(new Greeting().getCollection());
  collection.createIndex({ hrid: 1 }, { unique: true });
});
