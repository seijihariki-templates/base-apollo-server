import {
  Ctx, FieldResolver, Resolver, Root,
} from 'type-graphql';

import { IRequestContext } from '@/middleware/context';
import { Greeting } from './Greeting.model';

@Resolver((of) => Greeting)
export class GreetingModel {
  @FieldResolver()
  public representation(
  @Ctx() ctx: IRequestContext,
    @Root() greeting: Greeting,
  ) {
    return `[${greeting.hrid}]: ${greeting.template}`;
  }
}
