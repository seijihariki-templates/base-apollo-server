import { Field, InputType } from 'type-graphql';
import { Role } from './User.model';

@InputType({ description: 'User Creation Input data' })
export class UserCreateInput {
  @Field({ description: "User's email address" })
  public email: string;

  @Field({ description: "User's password" })
  public password: string;

  @Field((type) => [Role], { description: "User's roles" })
  public roles: Role[];
}
