import { getDB } from '@/dataloader/db';
import { Indexable } from '@/model/Indexable';
import bcryptjs from 'bcryptjs';
import { Field, ObjectType, registerEnumType } from 'type-graphql';

export enum Role {
  READ = 'READ',
  WRITE = 'WRITE',
  ADMIN = 'ADMIN',
}

registerEnumType(Role, { name: 'Role', description: 'Role identifier' });

@ObjectType({ description: 'Represents a user' })
export class User extends Indexable<User> {
  @Field({ description: "User's e-mail address" })
  public email: string;

  @Field((type) => [Role], { description: "User's roles" })
  public roles: Role[];

  // User's password bcrypt hash
  set password(pass: string) {
    this.hash = bcryptjs.hashSync(pass, 10);
  }

  private hash: string;

  // Verifies password;
  public verifyPassword(pass: string) {
    return bcryptjs.compareSync(pass, this.hash);
  }

  // Required
  public getCollection() {
    return 'users';
  }
}

getDB().then(({ db }) => {
  const collection = db.collection(new User().getCollection());
  collection.createIndex({ email: 1 }, { unique: true });
});
