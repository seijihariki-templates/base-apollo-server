import { ApolloServer, Config, CorsOptions } from 'apollo-server';
import 'reflect-metadata';

import { context } from '@/middleware/context.make';
import { logger } from './logging';
import { schema } from './schema';

const port = process.env.PORT || 3000;

// configure the server here
const serverConfig: Config & {
  cors?: CorsOptions | boolean,
} = {
  context,
  cors: process.env.CORS_ORIGIN && {
    credentials: true,
    origin: process.env.CORS_ORIGIN,
  },
  introspection: true,
  playground: {
    settings: {
      'editor.theme': 'dark', // change to light if you prefer
    },
  },
  schema,
};

// create a new server
const server = new ApolloServer(serverConfig);
server
  .listen(port)
  .then(({ url }) => {
    logger.info(`Server ready at ${url}`);
  })
  .catch((reason) => {
    logger.error('Server failed to start!', reason);
  });
