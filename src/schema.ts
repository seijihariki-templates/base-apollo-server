import { buildSchemaSync, MiddlewareFn, NonEmptyArray } from 'type-graphql';

// Middlewares
import { authChecker } from './auth/auth.checker';
import { logger } from './logging';
import { IRequestContext } from './middleware/context';

// Resolvers
const resolvers = [];
const resolverContext = require.context('.', true, /\.resolver\.ts$/);

resolverContext.keys().forEach((key) => {
  logger.info(`Loading resolver: ${key}`);
  resolvers.push(resolverContext(key));
});

// Middlewares
const middlewares = [];
const middlewareContext = require.context('.', true, /\.middleware\.ts$/);

middlewareContext.keys().forEach((key) => {
  logger.info(`Loading middleware: ${key}`);
  middlewares.push(middlewareContext(key));
});

if (resolvers.length < 1) {
  throw new Error('No resolvers were found!');
}

// Middlewares
export const schema = buildSchemaSync({
  authChecker,
  globalMiddlewares: middlewares.map((o) => Object.values<MiddlewareFn<IRequestContext>>(o)[0]),
  resolvers: <NonEmptyArray<Function> | NonEmptyArray<string>>
    resolvers.map((o) => o[Object.keys(o)[0]]),
});
