import { ApolloError } from 'apollo-server';
import { MongoError } from 'mongodb';

const handlers = {
  11000: (params: any) => {
    const keys = Object.keys(params.keyPattern);
    throw new ApolloError(`Key${keys.length > 1 ? 's' : ''} [${keys.join(', ')}] must be unique`, '409');
  },
};

export const handleMongoError = (e: Error, params?: any) => {
  if (e instanceof MongoError) {
    handlers[e.code](params || e);
  }
};
